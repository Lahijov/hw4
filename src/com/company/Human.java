package com.company;

import java.util.Arrays;

public class Human {
    String name;
    String surname;
    int year;
    int iq;
    Pet pet;
    Human mother;
    Human father;
    String[][] scedule = new String[7][2];

    public Human(String name, String surname, int year, Human mother, Human father) {
        this.name = name;
        this.surname = surname;
        this.year = year;
        this.mother = mother;
        this.father = father;
    }

    public void greetPet() {
        System.out.println("Hello," + pet.nickname);
    }

    public void describePet() {
        System.out.print("I have a " + pet.species + "," + "he is " + pet.age +
                " years old,he is ");
        if (iq < 50) {
            System.out.println("almost not sly");
        } else if (iq > 50) {
            System.out.println("very sly");
        }
    }
    @Override
    public String toString() {
        return "Human{" +
                "name='" + name + '\'' +
                ", surname='" + surname + '\'' +
                ", year=" + year +
                ", iq=" + iq +
                ", mother=" + mother.name +" "+mother.surname+
                ", father=" + father.name +" "+father.surname+
                ", pet=" +"dog{" +
                "nickname='" + pet.nickname + '\'' +
                ", age=" + pet.age +
                ", trickLevel=" + pet.trickLevel +
                ", habits=" + Arrays.toString(pet.habits) + '}' +
                '}';
    }

    public Human(String name, String surname, int year) {
        this.name = name;
        this.surname = surname;
        this.year = year;
    }


    public Human() {
    }
    public Human(String name, String surname, int year, int iq, Pet pet, Human mother, Human father, String[][] scedule) {
        this.name = name;
        this.surname = surname;
        this.year = year;
        if(iq<=100 || iq>=1){
            this.iq = iq;}
        else{
            System.out.println("Your number is higher or smaller than 100 and 1,respectively.So,iq level is set 99 by system");
            this.iq=99;
        }

        this.pet = pet;
        this.mother = mother;
        this.father = father;
        this.scedule = scedule;
    }
}

