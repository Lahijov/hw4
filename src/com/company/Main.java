package com.company;

public class Main {

    public static void main(String[] args) {
        //Checking all constructors
//        Pet pet3 = new Pet("pitbull");
//        Pet pet2 = new Pet("doban", "daniel", 6, 43, new String[]{"fast", "active"});
//
//        Human person = new Human("Ali", "Aliyev", 23);
//        Human person1 = new Human("Zeyneb", "Caniyeva", 29, new Human("Sevda", "Caniyeva", 44), new Human("Natig", "Caniyev", 45));
//        Human person2 = new Human("Rafig", "Lahijov", 18, 79, new Pet("doban", "daniel", 6, 43, new String[]{"fast", "active"}), new Human("Sevda", "Caniyeva", 44), new Human("Natig", "Caniyev", 45),
//                new String[][]{{"Go home"}, {"Play phone"}});


//Creating family
        Pet pet1 = new Pet("doban", "daniel", 6, 43, new String[]{"fast", "active"});
        Human mother = new Human("Aysel", "Sevdayeva", 60);
        Human father = new Human("Seymur", "Sevdayev", 60);
        Human child = new Human("Samir", "Sevdayev", 18, 59, pet1, mother, father,
                new String[][]{{"Buy phone"}, {"Play gitar"}});

        //Methods of Human
        System.out.println(child.toString());
        child.describePet();
        child.greetPet();
        //methods of Pet
        System.out.println(pet1.toString());
        pet1.eat();
        pet1.foul();
        pet1.respond();


    }
}
